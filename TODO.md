TODO list for PDL LainOS
Beta Version 2022 - PDL 1.2
LainOS Team

- Add Program Remove Panel [ ]
- Fix some dangerous bug [++]
- Check files type [+]
- Connect PDL with LainOS website * [ ]
- Add & parse Description from PDL patch file and save it in the DB [ ]
- make more than 1 repo for users ( users can set more than 1 repo ) []
- Download pathc files from repo based on branch []
